const winston = require('winston');

module.exports = {
    winston: winston.createLogger({
        level: 'debug',
        format: winston.format.simple(),
        transports: [
            new winston.transports.Console({
                stderrLevels: ['silly', 'debug', 'verbose', 'info', 'warn', 'error'],
                level: 'warn',
                format: winston.format.colorize({ all: true,  })
            }), 
            new winston.transports.File({
                filename:"evola.log",
                level: 'debug'
            })
        ]
    })
}
