const { basename } = require('path');
const { Readable } = require('stream');
const { winston } = require('../winston');

module.exports = {
    /**
     *
     * @param {import('../normalize').NormalizedData} data
     * @param {Record<string, string>} options
     * @returns {Readable}
     */
    default(data, options) {

        return new PsqlScriptStream(data, options);
    }
}

class PsqlScriptStream extends Readable {
    constructor(data, userName, resultInfo, options) {
        super();

        this.filename = basename(options._[1]);
        this.rows= data.rows;
        this.importId = data.importId;
        this.options = options;
        this.iter = this._readLine(); 
        this.user = userName;
        this.resultInfo = resultInfo.result;
    }

    _read() {
        const iteration = this.iter.next();
        this.push(iteration.done ? null : iteration.value);
    }

    *_readLine() {
        
        yield 'delete from etl_matrice m using imports i where m.import = i.id and i.type = \'EVOLME\';\n';
        yield `insert into imports(id, type, "timestamp", filename, starting_user) values ('${this.importId}', 'EVOLME', NOW(), '${this.filename}', '${this.user}',);\n`;  
        yield `insert into imports_status(import_id, import_step, import_status, "timestamp", result_information, "user") values ('${this.importId}', 'check_file', 'success', NOW(), '${this.resultInfo.errorDescription}', '${this.user}',);\n`; 
        const source = this.options.writeDataFile? this.options.dataFile.path: "STDIN" 
        yield `copy etl_matrice(${this.rows[0].join(', ')}) from '${source}';\n`;

        winston.info('Converting data into PSQL copy data…');
        if(this.options.dataFile){   
            winston.info(`writing evolme data  to seperate file '${source}' `);
            //write lines to be read to file, so psql can import them from this file 

            for (let rowIndex = 1; rowIndex < this.rows.length; rowIndex++) {
                const rowData = this.rows[rowIndex];
                this.rows[rowIndex] = null;
                this.options.dataFile.write(rowData.join('\t') + '\n', "utf-8");
                }    
                //when PSQL executes COPY from external file, it does not need the terminating .\ that it needs otherwise 

            
        } 
        else{  
            winston.info(`evolme data will at import time be read from '${source}' `);
            
            for (let rowIndex = 1; rowIndex < this.rows.length; rowIndex++) {
                const rowData = this.rows[rowIndex];
                this.rows[rowIndex] = null;
                yield rowData.join('\t') + '\n';
                }  
         yield '\\.\n\n';
        }

  
            
    }
}
