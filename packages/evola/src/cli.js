const { createReadStream, createWriteStream } = require('fs');
const { EventEmitter } = require('events');
const { noop } = require('lodash/fp');
const yargs = require('yargs');
const { transform } = require('./transform');
const {checkImport} = require('./check');
const { winston } = require('./winston');
const { getCliArguments, initSrcAndDest } = require('../../../common/clitools');

module.exports = {
    async runCli() {
        preventNodeExit();

        const argv = yargs
            .alias('t', 'transformation')
            .default('transformation', 'psql')
            .argv;

            let arglen = argv._.length;
            winston.debug( `runCli, Argument length: ${arglen}`);
    
            if( arglen !== 6){
                winston.error( `evola, 6 arguments required, given: ${arglen}`)
                if( arglen===5)
                winston.error( `args: ${argv._[0]} ${argv._[1]} ${argv._[2]}  ${argv._[3]} ${argv._[4]}`)
                return -1;
            }
    
            const arguments = getCliArguments(yargs.argv._);
            winston.debug( `runCli, Arguments: ${arguments}`);
          
            const commandName = arguments.commandName;
            const userName = arguments.userName;
            const importId = arguments.importId;
            const fileStructure = arguments.fileStructure;
    
            winston.debug( `cli command: ${commandName}`);
            winston.debug( `cli userName: ${userName}`);
            winston.debug( `cli importId: ${importId}`);
    
        if( commandName === "start_import"){
            // console.log( "runCli start_import")
            const { src, dest } = getSrcAndDest(yargs.argv._);
            await transform(src, dest, argv);    
        }
        else if( commandName === "check_import"){
            // console.log( "runCli check_import")
            const { src, dest } = getSrcAndDest(yargs.argv._);
            await checkImport(src, dest, argv);
        }
        else{
            // todo handle wrong command
            console.log( "runCli wrong command")
        }


        allowNodeExit();
    }
};

const eventEmitter = new EventEmitter();

function preventNodeExit() {
    eventEmitter.emit('block', { intention: 'block' });
}

function allowNodeExit() {
    eventEmitter.on('block', noop);
}

function getCommandName(args){
    const command = yargs.argv._[0];
    return command;
}

function getSrcAndDest(args) {
    const srcArg = yargs.argv._[1];
    const src = createReadStream(srcArg);
    const destArg = yargs.argv._[4];
    const dest = !destArg
        ? destFromSource(srcArg)
        : destArg === '-'
        ? process.stdout
        : createWriteStream(destArg);

    return { src, dest };
}

/**
 *
 * @param {string} src
 */
function destFromSource(src) {
    const base = /^(.*\/)?([^\\\/]*?)(\.[^.\\\/]*)?$/.exec(src);
    return createWriteStream(`${base[1]}${base[2]}.sql`);
}
