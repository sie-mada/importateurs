const { ReadStream } = require('fs');
const { Readable, Writable } = require('stream');
const path = require("path");
const sharedVolumePath = "/var/lib/meeh-shell/importers/data/"
const { createReadStream, createWriteStream } = require('fs');
const CombinedStream = require('combined-stream2');
const { winston } = require('./winston');
const isStream = require('is-stream');

module.exports = {
    /**
     *
     * @param {Readable} src
     * @param {Writable} dest
     * @param {Record<string, unknown>} options
     */

    async checkImport(src, dest, options) {
        // todo fill code for checking files for consistency herr
        //console.log( "todo checkImport")
        const transformation = typeof options.transformation === 'string' ? options.transformation.trim() : 'psql';
        if (src instanceof ReadStream) {
            winston.info(`Reading source file "${src.path}"…`);
        } else {
            winston.info('Reading source stream…');
        }

        const returnInfo = {
            result: "done",
            errorDescription: ""
        }
        const normalized = null; // todo change normalized to psql data when performing check
        return emptyOutput(dest, transformation, options, src, returnInfo);
    }

}

/**
 *
 * @param {import('./normalize').NormalizedData} data
 * @param {Writable} dest
 * @param {string} transformation
 * @param {Record<string, unknown>} options
 */
async function output(data, dest, transformation, options,src, returnInfo) { 
    if(dest===process.stdout){   
        const extension = path.extname(src.path);
        const insertLinesPath =  sharedVolumePath+ path.basename(src.path, extension) + ".sql";
        const insertLinesStream = createWriteStream(insertLinesPath);  
        options.writeDataFile =true; 
        options.dataFile= insertLinesStream; 
    }
    await new Promise((resolve, reject) => {
        const str = JSON.stringify(returnInfo);
        const delimiter = "_endOfInfo_";
        const str2 = str.concat(delimiter);
        const infoStream = Readable.from([str2]);

        const outData = loadTransformation(transformation).default(data, options);
        if (isStream.readable(outData)) {
            let combinedStream = CombinedStream.create();
            combinedStream.append(infoStream);
            combinedStream.append(outData);
            combinedStream.pipe(dest);
            dest.on('finish', () => {
                resolve();
            });
            outData.on('error', () => {
                reject();
            });
        } else {
            dest.write(
                outData,
                'utf8',
                err => err ? reject(err) : resolve()
            );
        }
    });
    await new Promise(resolve => dest.end(resolve));
}

/**
 *
 * @param {import('./normalize').NormalizedData} data
 * @param {Writable} dest
 * @param {string} transformation
 * @param {Record<string, unknown>} options
 */
async function emptyOutput(dest, transformation, options,src, returnInfo) { 
    if(dest===process.stdout){   
        const extension = path.extname(src.path);
        const insertLinesPath =  sharedVolumePath+ path.basename(src.path, extension) + ".sql";
        const insertLinesStream = createWriteStream(insertLinesPath);  
        options.writeDataFile =true; 
        options.dataFile= insertLinesStream; 
    }
    await new Promise((resolve, reject) => {
        const str = JSON.stringify(returnInfo);
        const delimiter = "_endOfInfo_";
        const str2 = str.concat(delimiter);
        const infoStream = Readable.from([str2]);
        //let combinedStream = CombinedStream.create();
        //combinedStream.append(infoStream);
        infoStream.pipe(dest);
        dest.on('finish', () => {
            resolve();
        });
        winston.debug('output done');
    });
    await new Promise(resolve => dest.end(resolve));
}

/**
 *
 * @param {string} transformation
 */
function loadTransformation(transformation) {
    try {
        return require(`./transformations/${transformation}`);
    } catch (e) {
        throw new Error(`Failed to load transformation ${transformation}`);
    }
}
