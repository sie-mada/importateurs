/** Utilisé pour l'appliquation de plusieurs modification au même valeur, par ex.:
 * `flow(monthly(), fac(1000))` */
function flow(...fns) {
    return function(val) {
        fns.reduce((intermediate, fn) => fn(intermediate), val);
    };
}

/** Diviser par 12 pour arriver à un valeur moyen mensuel. Cette modification doit être appliquée
 * où un moyen peut être calculé de cette façon (par ex. pour la production) mais il faut
 * l'éviter pour des autres valeur où une telle calculation n'est pas faisable (par ex. la
 * puissance). */
function monthly() {
    return function(val) {
        return val / 12;
    };
}

/** Appliquer un valeur par défaut où le valeur existant ne peut pas être automatiquement converti
 * en forme numérique. Par ex., `def(0)` va appliquer le valeur `0` oùun valeur est manquand. */
function def(defVal) {
    return function(val) {
        return isNaN(+val) ? defVal : val;
    };
}

/** Appliquer une division. Il s'agit d'une opération applicable à une ligne où `field` est une
 * liste de deux listes et la somme des valeurs de la premièmre liste sera divisée par la somme des
 * valeurs de la deuxième liste. Par ex.: Si `field` est défini comme
 * `[['revenueBTPMEIPrivate', 'revenueBTPMEIAdministration'], ['saleBTPMEIPrivate', 'saleBTPMEIAdministration']]`
 * `div()` divisera la somme des revenus par la somme des ventes pour arriver au prix moyen. */
function div() {
    return function([divident, divisor]) {
        return divisor ? (divident || 0) / divisor : 0;
    };
}

/** Multiplication par un facteur. Par ex. pour convertir les MW en kW utiliset `fac(1000)`,
 * ou pour l'inverse `fac(0.001)`. */
function fac(factor) {
    return function(val) {
        return val * factor;
    };
}

module.exports = {
    def,
    div,
    fac,
    flow,
    monthly
};
