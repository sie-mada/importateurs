/** Ingorer des lignes pertinentes aux réseaux interconnectés. */
function nonNet() {
    return function(baseDimensions) {
        return /^RI.$/.test(baseDimensions[2]);
    };
}

module.exports = {
    nonNet
};
