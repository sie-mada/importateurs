module.exports = {
    /**
     *
     * @param {string} cell
     */
    goRight(cell) {
        const parts = module.exports.getCellAddressParts(cell);

        return `${module.exports.incCol(parts.col)}${parts.row}`;
    },

    getCellAddressParts(cell) {
        const parts = /^([a-z]+)(\d+)$/i.exec(cell);
        if (!parts) {
            throw new Error(`Invalid cell address ${cell}`);
        }

        return { col: parts[1], row: parts[2] };
    },

    /**
     *
     * @param {string} col
     */
    incCol(col) {
        const buf = col.toUpperCase().split(new RegExp(''));
        let pos = col.length - 1;
        do {
            const chr = col[pos];
            if (chr !== 'Z') {
                buf[pos] = String.fromCharCode(chr.charCodeAt(0) + 1);
                return buf.join('');
            }

            buf[pos] = 'A';
            pos--;
        } while (pos >= 0);

        return `A${buf.join('')}`;
    },

    /**
     *
     * @param {string} a
     * @param {string} b
     */
    compareCols(a, b) {
        if (a.length - b.length) {
            return a.length - b.length;
        }

        return a > b
            ? 1
            : b > a
            ? -1
            :0;
    },

    getValueOrDefault(sheet, address, defaultValue) {
        const cell = sheet[address];

        return cell ? cell.v : defaultValue;
    }
};
