const { ReadStream } = require('fs');
const isStream = require('is-stream');
const { Readable, Writable } = require('stream');
const CombinedStream = require('combined-stream2');
const { v1: uuidV1 } = require('uuid');
const { read } = require('xlsx');
// const { FILE_STRUCTURE } = require('./definitions/file-structure');
const { normalize } = require('./normalize');
const { streamToBuffer } = require('./stream-to-buffer');
const { date1900 } = require('.//utils');
const { logger } = require('./logger'); 
const path = require("path");  
const { createWriteStream } = require('fs');
const { sharedVolumePath } = require('../../common/config-default');
const {addSheetToTableFrom, getEmptyTable} = require('./sheet')

module.exports = {
    /**
     *
     * @param {Readable} src
     * @param {Writable} dest
     * @param {Record<string, unknown>} options
     */
    async transform(src, dest, userName, importId, fileStructure, options) { 
        logger.info(`transform: ${src}, ${userName}, ${importId}, ${options.transformation}` );
        const transformation = typeof options.transformation === 'string' ? options.transformation.trim() : 'psql';
        if (src instanceof ReadStream) {
            logger.info(`transform. Reading source file ${src.path}…`);
        } else {
            logger.info('transform, Reading source stream…');
        }

        let book = await prepareWorkbook(src);
        logger.info(`transform, reparing input data from Excel workbook…`);
        let data = book.SheetNames
            .filter(name => /^(?:\d+|RI.)$/.test(name))
            .reduce(
                addSheetToTableFrom(book, fileStructure),
                getEmptyTable(fileStructure)
            );
        book = null;
        
        const normalized = normalize(data, importId);
        data = null; 
        const returnInfo = {
            result: "done",
            errorDescription: ""
        }
        logger.info('transform, start output');
        return output(normalized, dest, transformation, userName, options, src, returnInfo, importId);
    }
};

/**
 *
 * @param {Readable} src
 */
async function prepareWorkbook(src) {
    const srcBuf = await streamToBuffer(src);

    return read(srcBuf);
}

async function output (data, dest, transformation, userName, options, src, returnInfo, importId) {  
    //when process.stdout, we want to write the many lines in evolme to a seperate file 
    //that will be shared between the postgresql and the shell-backend container to speed up the import 
    if(dest===process.stdout){   
        const extension = path.extname(src.path);
        const insertLinesPath =  sharedVolumePath+ path.basename(src.path, extension) + ".sql";
        const insertLinesStream = createWriteStream(insertLinesPath);  
        options.writeDataFile =true; 
        options.dataFile= insertLinesStream; 
    }
    await new Promise((resolve, reject) => {

        const str = JSON.stringify(returnInfo);
        const delimiter = "_endOfInfo_";
        const str2 = str.concat(delimiter);
        const infoStream = Readable.from([str2]); 

     
        const outData = loadTransformation(transformation).default(data,userName, returnInfo, options, importId);


        if (isStream.readable(outData)) {
            let combinedStream = CombinedStream.create();
            combinedStream.append(infoStream);
            combinedStream.append(outData);
            combinedStream.pipe(dest);
            dest.on('finish', () => resolve());
            outData.on('error', reject);
        } else {
            dest.write (
                outData,
                'utf8',
                err => err ? reject(err) : resolve()
            );
        }
    });
    await new Promise(resolve => dest.end(resolve));
}

/**
 *
 * @param {string} transformation
 */
function loadTransformation(transformation) {
    try {
        return require(`./transformations/${transformation}`);
    } catch (e) {
        throw new Error(`Evolme Transform Failed to load transformation ${transformation}`);
    }
}
