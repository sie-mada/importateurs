const { basename } = require('path');
const { Readable } = require('stream');
const { logger } = require('../logger');

module.exports = {
    /**
     *
     * @param {import('../normalize').NormalizedData} data
     * @param {Record<string, string>} options
     * @returns {Readable}
     */
    default(data, userName, resultInfo, options, importId) {
        logger.debug( "psql_start_import");
        return new PsqlScriptStream(data, userName, resultInfo, options, importId);
    }
}

class PsqlScriptStream extends Readable {
    constructor(data, userName, resultInfo, options, importId) {
        super();

        this.filename = basename(options._[1]);
        this.rows= data.rows;
        this.importId = importId;
        this.options = options;
        this.iter = this._readLine();  
        this.user = userName;
        this.resultInfo =resultInfo;
 
    }

    _read() {
        const iteration = this.iter.next();
        this.push(iteration.done ? null : iteration.value);
    }

    *_readLine() {
        yield 'delete from etl_matrice m using imports i where m.import = i.id and i.type = \'EVOLME\';\n';
        yield `insert into imports_status(import_id, import_step, import_status, "timestamp", result_information, "user") values ('${this.importId}', 'import', 'done', NOW(), '${this.resultInfo.errorDescription}', '${this.user}');\n`; 
        const source = this.options.writeDataFile? this.options.dataFile.path: "STDIN" 
        yield `copy etl_matrice(${this.rows[0].join(', ')}) from '${source}';\n`;

        logger.info('Converting data into PSQL copy data…');
        if (this.options.dataFile) {   
            logger.info(`writing evolme data  to separate file '${source}' `);

            for (let rowIndex = 1; rowIndex < this.rows.length; rowIndex++) {
                const rowData = this.rows[rowIndex];
                this.rows[rowIndex] = null;
                this.options.dataFile.write(rowData.join('\t') + '\n', "utf-8");
            }
        } else {  
            logger.info(`evolme data will at import time be read from '${source}' `);
            
            for (let rowIndex = 1; rowIndex < this.rows.length; rowIndex++) {
                const rowData = this.rows[rowIndex];
                this.rows[rowIndex] = null;
                yield rowData.join('\t') + '\n';
            }  
            yield '\\.\n\n';
        }
    }
}
