const { basename } = require('path');
const { Readable } = require('stream');
const { logger } = require('../logger');

module.exports = {
    /**
     *
     * @param {import('../normalize').NormalizedData} data
     * @param {Record<string, string>} options
     * @returns {Readable}
     */
    default(data, userName, resultInfo, options, importId) {
        logger.debug( "psql_start_import_for_copy");
        return new PsqlScriptStream(data, userName, resultInfo, options, importId);
    }
}

class PsqlScriptStream extends Readable {
    constructor(data, userName, resultInfo, options, importId) {
        super();

        this.filename = basename(options._[1]);
        this.rows= data.rows;
        this.importId = importId;
        this.options = options;
        this.iter = this._readLine();  
        this.user = userName;
        this.resultInfo =resultInfo;
 
    }

    _read() {
        const iteration = this.iter.next();
        this.push(iteration.done ? null : iteration.value);
    }

    *_readLine() {
        yield 'delete from etl_matrice_copy m using imports i where m.import = i.id and i.type = \'EVOLME\';\n'; 
        //do not insert into imports_status because we are only inserting into copy table
        //yield `insert into imports_status(import_id, import_step, import_status, "timestamp", result_information, "user") values ('${this.importId}', 'import', 'done', NOW(), '${this.resultInfo.errorDescription}', '${this.user}');\n`; 
        //const source = this.options.dataFile.path
        const source = this.options.writeDataFile? this.options.dataFile.path: "STDIN" 
        yield `copy etl_matrice_copy(${this.rows[0].join(', ')}) from '${source}';\n`;

        logger.info('Converting data into PSQL copy data…');
   
            logger.info(`writing evolme data  to seperate file '${source}' `);

            for (let rowIndex = 1; rowIndex < this.rows.length; rowIndex++) {
                const rowData = this.rows[rowIndex];
                this.rows[rowIndex] = null;
                this.options.dataFile.write(rowData.join('\t') + '\n', "utf-8");
            
        }
    }
}
