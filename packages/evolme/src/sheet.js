const { getValueOrDefault, incCol } = require('./sheet-utils');
const { date1900 } = require('./utils');
const { v1: uuidV1 } = require('uuid');

module.exports = {

    /**
     *
     * @param {import('xlsx/types').WorkBook} book
     * @returns {(table: any[][], sheetName: string) => any[][]}
     */
    addSheetToTableFrom(book, fileStructure) {
        return function (table, sheetName) {
            const sheet = book.Sheets[sheetName];
            const sheetData = transformSheet(sheetName, sheet, fileStructure);
            table.push(...sheetData);

            return table;
        }
    },

    /**
     * @returns {any[][]}
     */
    getEmptyTable(fileStructure) {
        return [[
            'id',
            'areaId',
            ...fileStructure.map(field => field.property)
        ]];
    }

}

/**
 *
 * @param {string} sheetName
 * @param {import('xlsx/types').WorkSheet} sheet
 * @returns {any[][]}
 */

function transformSheet(sheetName, sheet, fileStructure) {
    const sheetData = [];

    const names = [sheetName];
    const initCol = findStartCol(sheet);

    for (let col = initCol; sheet[`${col}9`] && sheet[`${col}9`].v; col = incCol(col)) {
        const row = readMonth(sheet, col, names, fileStructure);
        sheetData.push(row);
    }

    return sheetData;
}

function findStartCol(sheet, currentCol = 'B', minDate1900 = date1900('2009-01-01')) {
    if (!sheet[`${currentCol}9`] || !sheet[`${currentCol}9`].v || sheet[`${currentCol}9`].v >= minDate1900) {
        return currentCol;
    }

    return findStartCol(sheet, incCol(currentCol), minDate1900);
}

/**
 *
 * @param {import('xlsx/types').WorkSheet} sheet
 * @param {string} col
 * @param {ReadonlyArray<string>} names
 * @returns {any[]}
 */
function readMonth(sheet, col, names, fileStructure) {
    /**
     * @type any[]
     */
    const general = [
        uuidV1(),
        ...names
    ];

    return fileStructure.reduce((dataRow, field) => (dataRow.push(getNumber(sheet, `${col}${field.row}`)), dataRow), general);
}

/**
 *
 * @param {import('xlsx/types').WorkSheet} sheet
 * @param {string} address
 * @returns {number | null}
 */
function getNumber(sheet, address) {
    const raw = getValueOrDefault(sheet, address, null);
    const numeric = +raw;

    return isNaN(numeric) || raw === undefined || raw === null || typeof raw === 'string' && !raw.trim()
        ? null
        : numeric;
}

