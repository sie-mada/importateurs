const { createWriteStream } = require('fs');
const { EventEmitter } = require('events');
const { noop } = require('lodash/fp');
const yargs = require('yargs');
const { transform } = require('./transform');
const { checkImport } = require('./check');
const { copyImport } = require('./copy-import')
const { logger } = require('./logger');
const fs = require('fs');
const { getCliArguments, initSrcAndDest } = require('../../common/clitools');
const { readFileStructure, pathForOutputForImportForKpi } = require('../../common/storage-utils');
const { fileStoragePath } = require('../../common/config-default'); 
const path = require("path");  

module.exports = {
    async runCli() {
        preventNodeExit();

        const argv = yargs
            .alias('t', 'transformation')
            .default('transformation', 'psql')
            .argv;

        let arglen = argv._.length;
        logger.debug( `runCli, Argument length: ${arglen}`);

        if( arglen !== 7){
            logger.error( `evolme, 7 arguments required, given: ${arglen}`)
            return -1;
        }

        const arguments = getCliArguments(yargs.argv._);
        logger.debug( `runCli, Arguments: ${arguments}`);
      
        const commandName = arguments.commandName;
        const userName = arguments.userName;
        const importId = arguments.importId;
        const fileStructurePath = arguments.fileStructure;
        const checkFile = arguments.checkFile;

        logger.debug( `cli command: ${commandName}`);
        logger.debug( `cli userName: ${userName}`);
        logger.debug( `cli importId: ${importId}`);
        logger.debug( `cli fileStrucurePath: ${fileStructurePath}`);

        if (commandName === 'start_import') {
            logger.info( "runCli start_import")
            const { src, dest } = initSrcAndDest( arguments.srcArg, arguments.destArg);
            argv.transformation = argv.transformation + "_" + commandName;
            logger.info( 'start_import, transformation: ', argv.transformation);
            logger.debug( `cli, read file structure for ${importId} from ${fileStoragePath}`)
            const fileStructureDefinition = readFileStructure(importId, fileStoragePath);
            logger.debug('fileStructureDefinition:');
            logger.debug(`${fileStructureDefinition}`);
            await transform(src, dest, userName, importId, fileStructureDefinition, argv);
            logger.debug( "start_import, transform finished");
        } 
        else if (commandName === 'start_import_for_copy') {
            logger.info( "runCli start_import_for_copy")
            const { src, dest } = initSrcAndDest( arguments.srcArg, arguments.destArg);  
            let [fileStructure, fileStructureFrench] = getFileStructuresFromFile(fileStructurePath);
            argv.transformation = argv.transformation + "_" + commandName; 
            logger.info( `start_import_for_copy, transformation: ${argv.transformation}`);
            await copyImport(src, dest, userName, importId, fileStructure, fileStructureFrench, checkFile, argv);
            logger.debug( "start_import_for_copy, transform finished");
        }
        else if ( commandName === 'check_import'){
            logger.info( "runCli check_import"); 
            const { src, dest } = initSrcAndDest( arguments.srcArg, arguments.destArg);  
            let [fileStructure, fileStructureFrench] = getFileStructuresFromFile(fileStructurePath);
            argv.transformation = argv.transformation + "_" + commandName; 
            logger.info( `check_import, transformation: ${argv.transformation}`);
            await checkImport(src, dest, userName, importId, fileStructure, fileStructureFrench, checkFile, argv);
            logger.debug( "check_import, transform finished");
;        }
        else {
            // todo handle wrong command
            logger.error( "runCli wrong command");

            return -1;
        }
        allowNodeExit();
    }
};

const eventEmitter = new EventEmitter();

function preventNodeExit() {
    eventEmitter.emit('block', { intention: 'block' });
}

function allowNodeExit() {
    eventEmitter.on('block', noop);
}

function IsJsonString(str) {
    try {
      var json = JSON.parse(str);

      return (typeof json === 'object');
    } catch (e) {

      return false;
    }
  }

/**
 *
 * @param {string} src
 */
function destFromSource(src) {
    const base = /^(.*\/)?([^\\\/]*?)(\.[^.\\\/]*)?$/.exec(src);
    return createWriteStream(`${base[1]}${base[2]}.sql`);
}

function getFileStructuresFromFile (filePath) {
    let fileStructure = {};
    let fileStructureFrench = {};
    let rawData = fs.readFileSync(filePath);
    const object = JSON.parse(rawData);
    // logger.debug("getFileStructuresFromFile object:------------------");
    // logger.debug(object);
    fileStructureFrench = object.FileStructureFrench;
    fileStructure = object.FileStructure;
    //    logger.debug("getFileStructuresFromFile File Structure:------------------");
    //    logger.debug(`${fileStructure}`);
    //    logger.debug("getFileStructuresFromFile File Structure French:------------------");
    //    logger.debug(fileStructureFrench);

    return [fileStructure, fileStructureFrench];
}
