const isStream = require('is-stream');
const { Readable, Writable } = require('stream');
const CombinedStream = require('combined-stream2');
const { read } = require('xlsx');
const { normalize } = require('./normalize');
const { getValueOrDefault, incCol } = require('./sheet-utils');
const { streamToBuffer } = require('./stream-to-buffer');
const { logger } = require('./logger');  
const path = require("path");  
const { createReadStream, createWriteStream, ReadStream } = require('fs');
const { sharedVolumePath, fileStoragePath } = require('../../common/config-default');
const {addSheetToTableFrom, getEmptyTable} = require('./sheet'); 
const {checkFieldsInCorrectRow, checkMonthsSameForAllSheets} = require("./check_functions");
const { v1: uuidV1 } = require('uuid');
const { dropLast } = require('lodash/fp');
const { saveExcelFile, saveFileStructure } = require('../../common/storage-utils');

module.exports = {

    /**
     *
     * @param {Readable} src
     * @param {Writable} dest
     * @param {string} userName
     * @param {string} importId
     * @param {Record<string, unknown>} options
     */ 

    async copyImport (src, dest, userName, importId, fileStructure, fileStructureFrench, checkFile, options) {
        logger.info(`copyImport: ${src}, ${userName}, ${importId}, ${options.transformation}` );   
        const transformation = typeof options.transformation === 'string' ? options.transformation.trim() : 'psql';
        if (src instanceof ReadStream) {
            logger.info(`Reading transform source file ${src.path}…`);
        } else {
            logger.info('Reading transform source stream…');
        }

        let book = await prepareWorkbook(src);
        logger.info(`copyImport Preparing input data from Excel workbook…`);
        let data = book.SheetNames
            .filter(name => /^(?:\d+|RI.)$/.test(name))
            .reduce(
                addSheetToTableFrom(book, fileStructure),
                getEmptyTable(fileStructure)
            );
        book = null;
        
        const normalized = normalize(data, importId);
        data = null; 
        const returnInfo = {
            result: "done",
            errorDescription: ""
        }
        logger.info('copyImport, start output');
        return output(normalized, dest, transformation, userName, options, src, returnInfo, importId);

        // logger.debug( 'copyImport, starting output');
        // saveFileStructure( fileStructure, fileStructureFrench, importId, fileStoragePath);
        // saveExcelFile( importId, src.path, fileStoragePath);
        // return output(importId, dest, transformation, userName, options, src, returnInfo);
    }
} 

/**
 *
 * @param {Readable} src
 */
async function prepareWorkbook(src) {
    const srcBuf = await streamToBuffer(src);

    return read(srcBuf);
}    


/**
 *
 * @param {Writable} dest
 * @param {string} transformation
 * @param {Record<string, unknown>} options
 */
async function output(data, dest, transformation, userName, options,src, returnInfo, importId) { 
    logger.debug("copy import output");
    if(dest===process.stdout){   
        const extension = path.extname(src.path);
        const insertLinesPath =  sharedVolumePath+ path.basename(src.path, extension) + ".sql";
        const insertLinesStream = createWriteStream(insertLinesPath);  
        options.writeDataFile =true; 
        options.dataFile= insertLinesStream; 
    }
    await new Promise((resolve, reject) => {

        const str = JSON.stringify(returnInfo);
        const delimiter = "_endOfInfo_";
        const str2 = str.concat(delimiter);
        const infoStream = Readable.from([str2]);
        logger.debug("output, loading transformation...");
        const outData = loadTransformation(transformation).default(data,userName, returnInfo, options, importId);
        logger.debug("output, transformation loaded");
        if (isStream.readable(outData)) {
            let combinedStream = CombinedStream.create();
            combinedStream.append(infoStream);
            combinedStream.append(outData);
            combinedStream.pipe(dest);
            dest.on('finish', () => {
                logger.debug( "output, finish, resolving");
                resolve();
            });
            outData.on('error', () => {
                logger.debug( "output, error, rejecting");
                reject();
            });
        } else {
            dest.write(
                outData,
                'utf8',
                err => err ? reject(err) : resolve()
            );
        }
        logger.debug('copy import output done');
    });

    await new Promise(resolve => dest.end(resolve));

}

/**
 *
 * @param {string} transformation
 */
function loadTransformation(transformation) {
    try {
        return require(`./transformations/${transformation}`);
    } catch (e) {
        throw new Error(`Evolme Copy Import: Failed to load transformation ${transformation}`);
    }
}
