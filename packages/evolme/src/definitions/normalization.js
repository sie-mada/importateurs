const { nonNet } = require('./normalization-conditions');
const { def, fac } = require('./normalization-modifications');

/**
 * @typedef {Object} Normalization
 * @property {string} code
 * @property {string | string[]} field
 * @property {Function=} modification
 * @property {(any) => boolean=} conditional
 */

/**
 * @type {Normalization[]}
 */
const NORMALIZATIONS = [
    { code: 'EL01', field: 'grossProduction' },
    { code: 'EL015HY', field: 'hydro' },
    { code: 'EL015CG', field: 'thermalGO' },
    { code: 'EL015CF', field: 'thermalFO' },
    { code: 'EL015ST', field: 'thermalSolar' },
    { code: 'EL015HB', field: 'hybrid' },
    { code: 'EL019', field: 'netProduction' },
    { code: 'EC133D', field: 'energyDelivery' },
    { code: 'DL08811', field: 'consumptionGO', modification: fac(1000) },
    { code: 'RF08811', field: 'consumptionFO', modification: fac(1000) },
    { code: 'EC13321', field: 'piHydro' },
    { code: 'EC13341EG', field: 'piThermalGO' },
    { code: 'EC13341EF', field: 'piThermalFO' },
    { code: 'EC13361', field: 'piThermalSolar' },
    { code: 'EC13391HB', field: 'piHybrid' },
    { code: 'EC13328', field: 'pdHydro' },
    { code: 'EC13348EG', field: 'pdThermalGO' },
    { code: 'EC13348EF', field: 'pdThermalFO' },
    { code: 'EC13368', field: 'pdThermalSolar' },
    { code: 'EC13398HB', field: 'pdHybrid' },
    { code: 'EC13390', field: 'maxProduction' },
    { code: 'EL121M', field: 'saleMTPrivate', conditional: nonNet() },
    { code: 'EL1235M', field: 'saleMTAdministration', conditional: nonNet() },
    { code: 'EL1214pM', field: 'saleMTPumps', conditional: nonNet() },
    { code: 'EL1235', field: 'saleBTPMEIPrivate', conditional: nonNet() },
    { code: 'EL1235A', field: 'saleBTPMEIAdministration', conditional: nonNet() },
    { code: 'EL1234I', field: 'saleBTInternal', conditional: nonNet() },
    { code: 'EL1214pB', field: 'saleBTPumps', conditional: nonNet() },
    { code: 'EL1231', field: 'saleBTResPrivate', conditional: nonNet() },
    { code: 'EL1231A', field: 'saleBTResAdministration', conditional: nonNet() },
    { code: 'EL1231J', field: 'saleBTResAgent', conditional: nonNet() },
    { code: 'EL1221L', field: 'saleLighting', conditional: nonNet() },
    { code: 'CO01', field: ['connectionsMTPrivate', 'connectionsMTAdministration', 'connectionsMTPumps'], modification: def(0), conditional: nonNet() },
    { code: 'CO0201', field: ['connectionsBTPMEIPrivate', 'connectionsBTPMEIAdministration', 'connectionsBTPMEIInternal', 'connectionsBTPMEIPumps'], modification: def(0), conditional: nonNet() },
    { code: 'CO0202', field: ['connectionsBTResPrivate', 'connectionsBTResAdministration', 'connectionsBTResAgent'], modification: def(0), conditional: nonNet() },
    { code: 'CO03', field: 'connectionsLighting', modification: def(0), conditional: nonNet() },
    { code: 'CN01', field: 'newConnectionsMT', conditional: nonNet() },
    { code: 'CN02', field: 'newConnectionsBT', conditional: nonNet() },
    { code: 'CUT01', field: 'cutsMT' },
    { code: 'CUT01H', field: 'cutsMTHours' },
    { code: 'CUT02', field: 'cutsBT' },
    { code: 'EP01', field: 'avgPriceMT', conditional: nonNet() },
    { code: 'EP0201', field: 'avgPriceBTPMEI', conditional: nonNet() },
    { code: 'EP0202', field: 'avgPriceBTRes', conditional: nonNet() },
];

module.exports = {
    NORMALIZATIONS
};
