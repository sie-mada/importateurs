const { logger } = require('./logger');  

const ALPHABET = ["A", "B", "C", "D", "E", "F", "G", "H", 
"I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S",
 "T", "U", "V", "W", "X", "Y", "Z"];

async function getMonthRow(fileStructure){  
    for(let rowEntryMapping of fileStructure){   
        let prop = rowEntryMapping["property"]; 
        if(prop==="month"){ 
            return rowEntryMapping["row"]
        }
    }
}  

async function getPrecedingColumn(lastCol){ 
    let lastChar = lastCol[lastCol.length-1]  ;
    let indexLastChar = lastCol.length-1;
    let precedingCol;
    if (lastChar!=="A") { 
        let precedingChar = ALPHABET[ALPHABET.indexOf(lastChar)-1] 
        precedingCol = lastCol.substr(0, indexLastChar) + precedingChar   
        return precedingCol;
    } else {  
        //lastCol = EFA -> EEZ
        let secondToLastChar = lastCol[indexLastChar-1];   
        
        if(secondToLastChar!=="A"){ 
            let precedingChar = ALPHABET[ALPHABET.indexOf(secondToLastChar)-1]    
            precedingCol = lastCol.substr(0, indexLastChar-2) + precedingChar  + "Z"        
        } else { 
            //very infrequent edge case, e.g EAA -> EZ
            let count = 1;   
            let index =indexLastChar -1 
            let appendString = "Z"
            while (secondToLastChar==="A") { 
                index = index -1;  
                if (index>-1) {
                    secondToLastChar  = lastCol[index];  
                    count = count +1;  
                    appendString = appendString + "Z"
                }
            } 
            precedingCol = lastCol.substr(0, index) + appendString        
        }

        return precedingCol;        
    }
}


async function getLastMonthWithEntry(sheet, monthRow){  
    // range of sheet is usually padded with null values, we want to determine the last column that actually contains data

    //range like "A1:FL228"
    let range = sheet["!ref"];   
    let lastEntry =range.split(":")[1]  
    logger.info(lastEntry);  
    for(var i = 0; i< lastEntry.length; i++){
        var entry = lastEntry[i]; 
        if (entry >= '0' && entry <= '9') {
            break
        }  
     } 
     let indexLastChar = i;  
     // returns FL 
     
     let lastCol = lastEntry.substr(0, i); 
     let monthValue = sheet[lastCol + monthRow]; 
     while(monthValue===undefined){ 
         lastCol =await getPrecedingColumn(lastCol);
         monthValue = sheet[lastCol + monthRow];
     } 

     return lastCol;
    
}

async function checkMonthsSameForAllSheets(book, fileStructure){  
    //TODO: check for wrong month row 
    let warnings = [];
    let sheets  = book.SheetNames;    
    let earliestMonth = null; 
    let latestMonth = null;  
    const monthRow = await getMonthRow(fileStructure); 
    const lastMonthColumn = await getLastMonthWithEntry(book.Sheets["TTZ"], monthRow); 
    let sheet = book.Sheets["TTZ"];  
    // .w = formatted
    earliestMonth = sheet["B" + monthRow]
    latestMonth  = sheet[lastMonthColumn + monthRow]; 
    
    for(let sheetname of sheets){ 
        let sheet = book.Sheets[sheetname]; 
        let firstMonth = sheet["B" + monthRow]  
        let lastMonth  = sheet[lastMonthColumn + monthRow]; 
        if(firstMonth.v!== earliestMonth.v || latestMonth.v !== lastMonth.v){  
            let errorString = `range of months in sheet ${sheetname} do not match up with other sheets `;   
            logger.error(errorString);
            warnings.push({
                errorType: 'wrong_cell_order',
                errorDescription: errorString
            });            
        } 

    }
    return warnings;    
} 

async function checkFieldsInCorrectRow(book, fileStructureFrench){
    let returnInfo = {
        severe: false,
        warnings: []
    }; 
    let firstSheet = book.Sheets["TTZ"];
    if(!firstSheet){
        logger.error("checkFieldsInCorrectRow: Sheet 'TTZ' is missing");
        returnInfo.warnings.push({
            errorType: 'wrong_sheets',
            errorDescription: 'Sheet TTZ is missing'
        });
        returnInfo.severe = true;
        
        return returnInfo;  // don't continue if sheet is missing
    }

    for(let rowEntryMapping of fileStructureFrench){  
        let row = rowEntryMapping["row"];  
        let property = rowEntryMapping["property"];
        let cellString = "A" + row
        let entry = firstSheet[cellString];    
        try {
            if(entry.t !=="s") {
                let errorString = `entry in cell ${cellString} is not a string`; 
                logger.error(errorString);
                returnInfo.warnings.push[errorString];
            }
            //entry is a dictionary, get raw value         
            else{ 
                let rawValue = entry["v"];   
                if(rawValue.trim()!==property){ 
                    let errorString = `entry in cell ${cellString} is expected to be ${property} but is ${rawValue.trim()}`;  
                    logger.error(errorString);
                    returnInfo.warnings.push({
                        errorType: 'wrong_row_order',
                        errorDescription: errorString
                    });
                }    
            } 
        } 
        catch(e) {
            let errorMessage = `error encountered when accessing cell ${cellString}, expected property ${property}, error:`;  
            errorMessage = errorMessage + String(e);
            returnInfo.warnings.push({
                errorType: 'wrong_cell_order',
                errorDescription: errorMessage
            });
        }
    } 
    return returnInfo;

}  



module.exports= { 
    checkFieldsInCorrectRow, 
    checkMonthsSameForAllSheets
}
