const { logger } = require('./logger');  

async function checkFieldsInCorrectRow(book, fileStructureFrench){
    let returnInfo = {
        severe: false,
        warnings: []
    }; 
     
    var first_sheet_name = book.SheetNames[0];
    if(!first_sheet_name){
        logger.error("checkFieldsInCorrectRow: Sheet is missing");
        returnInfo.warnings.push({
            errorType: 'wrong_sheets',
            errorDescription: 'First Sheet is missing'
        });
        returnInfo.severe = true;
        
        return returnInfo;  // don't continue if sheet is missing
    }

    const firstSheet = book.Sheets[first_sheet_name];

    for(let rowEntryMapping of fileStructureFrench){  
        let row = rowEntryMapping["row"];  
        let property = rowEntryMapping["property"];
        let cellString = "A" + row
        let entry = firstSheet[cellString];    
        try {
            if(entry.t !=="s") {
                let errorString = `entry in cell ${cellString} is not a string`; 
                logger.error(errorString);
                returnInfo.warnings.push[errorString];
            }
            //entry is a dictionary, get raw value         
            else{ 
                let rawValue = entry["v"];   
                if(rawValue.trim()!==property){ 
                    let errorString = `entry in cell ${cellString} is expected to be ${property} but is ${rawValue.trim()}`;  
                    logger.error(errorString);
                    returnInfo.warnings.push({
                        errorType: 'wrong_row_order',
                        errorDescription: errorString
                    });
                }    
            } 
        } 
        catch(e) {
            let errorMessage = `error encountered when accessing cell ${cellString}, expected property ${property}, error:`;  
            errorMessage = errorMessage + String(e);
            returnInfo.warnings.push({
                errorType: 'wrong_cell_order',
                errorDescription: errorMessage
            });
        }
    } 

    return returnInfo;
}  

module.exports= { 
    checkFieldsInCorrectRow
}
