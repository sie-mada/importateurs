const { basename } = require('path');
const { Readable } = require('stream');
const { logger } = require('../logger');

module.exports = {
    
    default(importId,userName, resultInfo, options) {
        logger.debug( "psql_check_import");
        return new PsqlScriptStream(importId, userName, resultInfo, options);
    }
}

class PsqlScriptStream extends Readable {
    constructor(importId, userName, resultInfo, options) {
        super();

        this.filename = basename(options._[1]);
        this.importId = importId;
        this.options = options;
        this.iter = this._readLine(); 
        this.user = userName;
        this.resultInfo = resultInfo; 
        
    }

    _read() {
        const iteration = this.iter.next();
        this.push(iteration.done ? null : iteration.value);
    }

    *_readLine() { 
        let errorDescription  = this.resultInfo.errorDescription;
        let errorString = "";
        for (let rowIndex = 0; rowIndex < errorDescription.length; rowIndex++) {
            errorString =  errorString + errorDescription[rowIndex] + "\n";
            }     
        
        
        //delete statement from etl_matrice is now in start_import only
        yield `insert into imports(id, type, "timestamp", filename, starting_user) values ('${this.importId}', 'ADER_MATRICE', NOW(), '${this.filename}', '${this.user}');\n`;  
        yield `insert into imports_status(import_id, import_step, import_status, "timestamp", result_information, "user") values ('${this.importId}', 'check_file', '${this.resultInfo.result}', NOW(), '${errorString}', '${this.user}');\n`;  
        //clean etl_matrice_copy in case of lingering data
        yield `delete from etl_matrice_copy;\n`;         
        //insert into copy table data from etl_matrice for kpi check 
        yield `insert into etl_matrice_copy select * from etl_matrice;\n`;        
        }

  
            
    }
 

