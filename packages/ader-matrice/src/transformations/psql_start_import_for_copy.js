const { basename } = require('path');
const { Readable } = require('stream');
const { logger } = require('../logger');

module.exports = {
    /**
     *
     * @param {import('../normalize').NormalizedData} data
     * @param {Record<string, string>} options
     * @returns {Readable}
     */
    default(data, userName, resultInfo, options, importId) {
        logger.debug( "psql_start_import_for_copy");
        return new PsqlScriptStream(data, userName, resultInfo, options, importId);
    }
}

class PsqlScriptStream extends Readable {
    constructor(data, userName, resultInfo, options, importId) {
        super();

        this.filename = basename(options._[1]);
        this.rows= data.rows;
        this.options = options;
        this.iter = this._readLine(); 
        this.year = options.year; 
        this.userName = userName 
        this.resultInfo = resultInfo; 
        this.importId = importId;
    }

    _read() {
        const iteration = this.iter.next();
        this.push(iteration.done ? null : iteration.value);
    }

    *_readLine() {  
        const cols = this.rows[0]; 
        const source = this.options.writeDataFile? this.options.dataFile.path: "STDIN" 
        yield `delete from etl_matrice_copy where etl_matrice_copy.import in (select distinct etl_matrice_copy.import from  
            etl_matrice_copy join imports on etl_matrice_copy.import = imports.id 
            WHERE "type" = 'ADER_MATRICE' AND date_part('year', mois) = ${this.year} );\n`;         //do not insert into imports_status because we are only inserting into copy table
        //yield `insert into imports_status(import_id, import_step, import_status, "timestamp", result_information, "user") values ('${this.importId}', 'import', 'done', NOW(), '${this.resultInfo.errorDescription}', '${this.user}');\n`; 

        logger.info('Converting data into PSQL copy data…');
   
        if(this.options.dataFile && false){   
            logger.info(`writing ader data  to seperate file '${source}' `); 

        yield `copy etl_matrice(${cols.map(col => col === 'societe' ? 'id_societe' : col).join(', ')})  from '${source}';\n `;
        //write lines to be read to file, so psql can import them from this file 

        for (let rowIndex = 1; rowIndex < this.rows.length; rowIndex++) { 

            const sep = rowIndex > 1 ? '\n' : '';
            const rowData = this.rows[rowIndex];
            const insertValues = buildInsertValues(cols, rowData);
            this.rows[rowIndex] = null;  
            this.options.dataFile.write( `${sep} (${insertValues.join(',')})`);

//                this.options.dataFile.write( `${sep} ${insertValues.join('\t')}`);

            }    
            //when PSQL executes COPY from external file, it does not need the terminating .\ that it needs otherwise 

            
        }  
        else {
            const cols = this.rows[0];
            yield `insert into etl_matrice_copy(${cols.map(col => col === 'societe' ? 'id_societe' : col).join(', ')}) values\n`;
        for (let rowIndex = 1; rowIndex < this.rows.length; rowIndex++) {
            const sep = rowIndex > 1 ? ',\n' : '';
            const rowData = this.rows[rowIndex];
            const insertValues = buildInsertValues(cols, rowData);
            this.rows[rowIndex] = null;
            yield `${sep} (${insertValues.join(',')})`;
        }

        yield ';\n\n'; 
        }
    }
}

/**
 *
 * @param {string[]} cols
 * @param {unknown[]} data
 */
function buildInsertValues(cols, data) {
    return cols
        .map((col, colIndex) => {
            const val = data[colIndex];
            if (col === 'societe') {
                return `(select id from company where name = '${('' + val).replace(/'/g, '\'\'')}')`;
            } else if (typeof val === 'string') {
                return `'${val.replace(/'/g, '\'\'')}'`;
            } else {
                return `${val}`;
            }
        });
} 
function buildInsertValuesFile(cols, data) {
            return cols
                .map((col, colIndex) => {
                                    const val = data[colIndex];
                                    if (col === 'societe') {
                                                            return `(select id from company where name = '${('' + val).replace(/'/g, '\'\'')}')`;
                                    } else if (typeof val === 'string') {
                                                            return `${val.replace(/'/g, '\'\'')}`;
                                    } else {
                                                            return `${val}`;
                                                        }
        });
}
