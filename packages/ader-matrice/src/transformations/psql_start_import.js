const { basename } = require('path');
const { Readable } = require('stream');
const { logger } = require('../logger');

const IMPORT_TYPE = 'ADER_MATRICE';

module.exports = {
    /**
     *
     * @param {import('../normalize').NormalizedData} data
     * @param {Record<string, string>} options
     * @returns {Readable}
     */
    default(data, userName, resultInfo, options, importId, companiesWithId, sites) {

        return new PsqlScriptStream(data, userName, resultInfo, options, importId, companiesWithId, sites);
    }
}

class PsqlScriptStream extends Readable {
    constructor(data, userName, resultInfo, options, importId, companiesWithId, sites) {
        super();

        this.filename = basename(options._[1]);
        this.rows= data.rows;
        this.options = options;
        this.iter = this._readLine(); 
        this.year = options.year; 
        this.userName = userName 
        this.resultInfo = resultInfo; 
        this.importId = importId; 
        this.companiesWithId = companiesWithId; 
        this.sites = sites;
    }

    _read() {
        const iteration = this.iter.next();
        this.push(iteration.done ? null : iteration.value);
    }

    *_readLine() { 
        const cols = this.rows[0]; 
        const source = this.options.writeDataFile? this.options.dataFile.path: "STDIN"; 

        yield `delete from etl_matrice where etl_matrice.import in (select distinct etl_matrice.import from  
            etl_matrice join imports on etl_matrice.import = imports.id 
            WHERE "type" = 'ADER_MATRICE' AND date_part('year', mois) = ${this.year} );\n`; 
        yield `insert into imports_status( import_id, import_step, import_status, "timestamp", result_information, "user") values ('${this.importId}', 'import', 'done', NOW(), '${this.resultInfo}', '${this.userName}' );\n`; 
        
        logger.info('Converting data into PSQL copy data…'); 

        if(this.options.dataFile && false){   
            logger.info(`writing ader data  to seperate file '${source}' `); 
            yield `copy etl_matrice(${cols.map(col => col === 'societe' ? 'id_societe' : col).join(', ')})  from '${source}';\n `;
            //write lines to be read to file, so psql can import them from this file 
            for (let rowIndex = 1; rowIndex < this.rows.length; rowIndex++) { 
                const sep = rowIndex > 1 ? '\n' : '';
                const rowData = this.rows[rowIndex];
                const insertValues = buildInsertValues(cols, rowData);
                this.rows[rowIndex] = null;  
                this.options.dataFile.write( `${sep} (${insertValues.join(',')})`);
                // this.options.dataFile.write( `${sep} ${insertValues.join('\t')}`);
            }    
            //when PSQL executes COPY from external file, it does not need the terminating .\ that it needs otherwise
        } else {
            const cols = this.rows[0];  
            if(this.companiesWithId.length > 0){ 
                yield `insert into company(id, "name") values\n`; 
                let length = this.companiesWithId.length; 
                for (let index=0; index < length; index++) { 
                    let company = this.companiesWithId[index]; 
                    const sep = index > 0 ? ',\n' : '';  
                    let name = company.name.replace(/'/g, '\'\''); 
                    yield `${sep} ('${company.id}' ,'${name}')`; 
                    } 
                yield ' ON CONFLICT DO NOTHING;\n'; 
            } 
            let length = this.sites.length;  
            yield `insert into centrales(id, societe, code ,nom, district_pcode) values\n`; 

            for( let index = 0; index < length; index ++){ 
                let site = this.sites[index];  
                const sep = index > 0 ? ',\n' : '';   
                let id = site.id;  
                let societe_name = site.societe_name.replace(/'/g, '\'\'');  
                let nome = site.nom.replace(/'/g, '\'\'');
                let code = site.nom.replace(/'/g, '\'\'');   
                let district = site.district.replace(/'/g, '\'\'');
                yield `${sep} ( '${id}', (select id from company where name = '${societe_name}') , '${code}' ,'${nome}', (select pcode from warehouse.district_names where name = '${district}' LIMIT 1) )`; 
            } 
            yield ' ON CONFLICT DO NOTHING;\n'; 
            yield `insert into etl_matrice(${cols.map(col => col === 'societe' ? 'id_societe' : col).join(', ')}) values\n`; 
            let collectString = "";
            for (let rowIndex = 1; rowIndex < this.rows.length; rowIndex++) {
                
                const rowData = this.rows[rowIndex];
                const insertValues = buildInsertValues(cols, rowData); 
                const sep = rowIndex > 1 ? ',\n' : '';
                this.rows[rowIndex] = null; 
                //gather all data first before yielding, because otherwise the app seems to crash in unpredictable ways
                collectString = collectString +`${sep} (${insertValues.join(',')})`;
            } 
            yield collectString;
            yield ';\n\n'; 
        }
    }
}

/**
 *
 * @param {string[]} cols
 * @param {unknown[]} data
 */
function buildInsertValues(cols, data) {
    return cols
        .map((col, colIndex) => {
            const val = data[colIndex];
            if (col === 'societe') {
                return `(select id from company where name = '${('' + val).replace(/'/g, '\'\'')}')`;
            } else if (typeof val === 'string') {
                return `'${val.replace(/'/g, '\'\'')}'`;
            } else {
                return `${val}`;
            }
        });
} 
function buildInsertValuesFile(cols, data) {
    return cols
        .map((col, colIndex) => {
            const val = data[colIndex];
            if (col === 'societe') {
                return `(select id from company where name = '${('' + val).replace(/'/g, '\'\'')}')`;
            } else if (typeof val === 'string') {
                return `${val.replace(/'/g, '\'\'')}`;
            } else {
                return `${val}`;
            }
    });
}

