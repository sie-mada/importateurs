const moment = require('moment');

/**
 * Creates an EXCEL 1900 date from a date input. An EXCEL 1900 date is the number of days since and including 1 January
 * 1900 (i.e. 1 January 1900 is 1). In addition, EXCEL incorretly considers the year 1900 a leap year so that an extra
 * day must be added for all dates from 1 March 1900 onwards.
 *
 * @param {import('moment').MomentInput} input
 * @returns {number}
 */
function date1900(input) {
    const base = moment([1900, 0, 1]);
    const m = moment(input);
    const daysDiff = m.diff(base, 'days');

    return daysDiff + 1 + +(daysDiff > 58);
}

module.exports = { date1900 };
