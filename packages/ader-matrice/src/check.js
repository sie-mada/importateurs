const isStream = require('is-stream');
const { Readable, Writable } = require('stream');
const CombinedStream = require('combined-stream2');
const XLSX = require('xlsx');
const { streamToBuffer } = require('./stream-to-buffer');
const { logger } = require('./logger');  
const path = require("path");  
const { createWriteStream, ReadStream } = require('fs');
const { sharedVolumePath, fileStoragePath } = require('../../common/config-default');
const {checkFieldsInCorrectRow} = require("./check-functions");
const { saveExcelFile, saveFileStructure } = require('../../common/storage-utils');

module.exports = {

    /**
     *
     * @param {Readable} src
     * @param {Writable} dest
     * @param {string} userName
     * @param {string} importId
     * @param {Record<string, unknown>} options
     */ 

    async checkImport (src, dest, userName, importId, fileStructure, fileStructureFrench, checkFile, options) {
        logger.info('checkImport ', src, userName, importId, checkFile, options.transformation);
        const transformation = typeof options.transformation === 'string' ? options.transformation.trim() : 'psql';
        if (src instanceof ReadStream) {
            logger.debug(`CheckImport, Reading check source file "${src.path}"…`);
        } else {
            logger.debug('CheckImport, Reading check source stream…');
        }

        let returnInfo;

        if (checkFile === 'false') {
            // This will be called when force import is selected
            logger.debug(`checkImport, Check skipped...`);  

            returnInfo = {
                result: "skipped",
                errorDescription: []
            }  
        }
        else {
            logger.debug(`checkImport, Preparing input data from Excel workbook…`);  
            let book = await prepareWorkbook(src);
            logger.debug(`checkImport, startCheckingFields …`);  
            let return1 = await checkFieldsInCorrectRow(book, fileStructureFrench);
            let warnings_two = [];
            // todo add further checks here in the way described below
            // if ( return1.severe===false) {
            //     warnings_two = await checkMonthsSameForAllSheets(book, fileStructure); 
            // }

            let warnings = [];
            if (warnings_two) {
                warnings = return1.warnings.concat(warnings_two);
            }
            else {
                warnings = return1.warnings;
            }
            if(warnings.length===0) {
                returnInfo = {
                    result: "done",
                    errorDescription: []
                } 
            } 
            else {
                returnInfo = {
                    result: "failed",
                    errorDescription: warnings
                } 
            } 
        }

        logger.debug( 'checkImport, starting output');
        saveFileStructure( fileStructure, fileStructureFrench, importId, fileStoragePath);
        saveExcelFile( importId, src.path, fileStoragePath);
        return output(importId, dest, transformation, userName, options, src, returnInfo);
    }
} 

/**
 *
 * @param {Readable} src
 */
async function prepareWorkbook(src) {
    const srcBuf = await streamToBuffer(src);

    return XLSX.read(srcBuf);
}    


/**
 *
 * @param {Writable} dest
 * @param {string} transformation
 * @param {Record<string, unknown>} options
 */
async function output(importId, dest, transformation, userName, options,src, returnInfo) { 
    logger.debug("output");
    if(dest===process.stdout){   
        const extension = path.extname(src.path);
        const insertLinesPath =  sharedVolumePath+ path.basename(src.path, extension) + ".sql";
        const insertLinesStream = createWriteStream(insertLinesPath);  
        options.writeDataFile =true; 
        options.dataFile= insertLinesStream; 
    }
    await new Promise((resolve, reject) => {

        const str = JSON.stringify(returnInfo);
        const delimiter = "_endOfInfo_";
        const str2 = str.concat(delimiter);
        const infoStream = Readable.from([str2]);
        logger.debug("output, loading transformation...");
        const outData = loadTransformation(transformation).default(importId,userName, returnInfo, options);
        logger.debug("output, transformation loaded");
        if (isStream.readable(outData)) {
            let combinedStream = CombinedStream.create();
            combinedStream.append(infoStream);
            combinedStream.append(outData);
            combinedStream.pipe(dest);
            dest.on('finish', () => {
                logger.debug( "output, finish, resolving");
                resolve();
            });
            outData.on('error', () => {
                logger.debug( "output, error, rejecting");
                reject();
            });
        } else {
            dest.write(
                outData,
                'utf8',
                err => err ? reject(err) : resolve()
            );
        }
        logger.debug('output done');
    });

    await new Promise(resolve => dest.end(resolve));

}

/**
 *
 * @param {string} transformation
 */
function loadTransformation(transformation) {
    try {
        return require(`./transformations/${transformation}`);
    } catch (e) {
        throw new Error(`Import Check Failed to load transformation ${transformation}`);
    }
}
