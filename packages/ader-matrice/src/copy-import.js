const { ReadStream } = require('fs');
const isStream = require('is-stream');
const { Readable, Writable } = require('stream');
const CombinedStream = require('combined-stream2');
const { v1: uuidV1 } = require('uuid');
const { logger } = require('./logger'); 
const path = require("path");  
const { createWriteStream } = require('fs'); 
const { read } = require('xlsx');
const { num } = require('./definitions/micro-transform'); 
const { str } = require('./definitions/micro-transform'); 
//const { FILE_STRUCTURE } = require('./definitions/file-structure');

const { normalize } = require('./normalize');
const { compareCols, incCol } = require('./sheet-utils');
const { streamToBuffer } = require('./stream-to-buffer');
const sharedVolumePath = "/var/lib/meeh-shell/importers/data/"

module.exports = {
    /**
     *
     * @param {Readable} src
     * @param {Writable} dest
     * @param {Record<string, unknown>} options
     */
    async copyImport(src, dest, userName, importId, fileStructure, fileStructureFrench, checkFile, options) {
        logger.info(`transform: ${src}, ${userName}, ${importId}, ${options.transformation}`);
        const transformation = typeof options.transformation === 'string' ? options.transformation.trim() : 'psql';
        if (src instanceof ReadStream) {
            logger.info(`transform, Reading source file "${src.path}"…`);
        } else {
            logger.info('transform, Reading source stream…');
        }

        let dataSheet = await prepareDataSheet(src);
        logger.info(`transform, reparing input data from Excel sheet…`);
        const year = +/\d+/.exec(dataSheet['A1'].v)[0]

        const companies = readCompanyList(dataSheet); 
        //todo: use real filestructure
        companies.forEach(comp => readSites(dataSheet, comp, fileStructure));
        let tables = buildTable(companies, year, fileStructure);
        const normalized = normalize(tables, importId);
        options.year = year
        dataSheet = tables = null;
        const returnInfo = {
            result: "done",
            errorDescription: ""
        }
        return output(normalized, dest, transformation, userName, options, src, returnInfo, importId);

    }
};

/**
 *
 * @param {Readable} src
 */
async function prepareDataSheet(src) {
    logger.debug(`prepareDataSheet`);
    logger.debug(`prepareDataSheet ${src}`);
    const srcBuf = await streamToBuffer(src);
    // logger.debug(`srcBuf: ${srcBuf}`);
    const book = read(srcBuf);
    logger.debug( `book: ${book}`);
    logger.debug( `sheets: ${book.SheetNames}`);
    const statSheet = book.SheetNames.filter(name => /stat expl/i.test(name));
    if (!statSheet.length) {
        throw new Error('Data sheet not found');
    } else if (statSheet.length > 1) {
        throw new Error(`More than one candidate sheet found (${statSheet.join(', ')})`);
    }

    return book.Sheets[statSheet[0]];
}

async function output (data, dest, transformation, userName, options,src, returnInfo, importId) { 
    if(dest===process.stdout){   
        //when process.stdout, we want to write the many lines in evolme to a seperate file 
        //that will be shared between the postgresql and the shell-backend container to speed up the import 
        const extension = path.extname(src.path);
        const insertLinesPath =  sharedVolumePath+ path.basename(src.path, extension) + ".sql";
        const insertLinesStream = createWriteStream(insertLinesPath);  
        options.writeDataFile =true; 
        options.dataFile= insertLinesStream; 
    }
    await new Promise((resolve, reject) => {

        const str = JSON.stringify(returnInfo);
        const delimiter = "_endOfInfo_";
        const str2 = str.concat(delimiter);
        const infoStream = Readable.from([str2]);

        const outData = loadTransformation(transformation).default(data, userName, returnInfo, options, importId);

        if (isStream.readable(outData)) {
            let combinedStream = CombinedStream.create();
            combinedStream.append(infoStream);
            combinedStream.append(outData);
            combinedStream.pipe(dest);
            dest.on('finish', () => {
                resolve();
            });
            outData.on('error', () => {
                reject();
            });
        } else {
            dest.write (
                outData,
                'utf8',
                err => err ? reject(err) : resolve()
            );
        }
    });
    await new Promise(resolve => dest.end(resolve));
}

/**
 *
 * @param {string} transformation
 */
function loadTransformation(transformation) {
    try {
        return require(`./transformations/${transformation}`);
    } catch (e) {
        throw new Error(`Failed to load transformation ${transformation}`);
    }
}

function readCompanyList(sheet) {
    const companies = [];

    const finalCol = /:([A-Z]+)/.exec(sheet['!ref'])[1];
    for (let col = 'B'; compareCols(col, finalCol) <= 0; col = incCol(col)) {
        const cell = sheet[`${col}3`];
        if (cell) {
            companies.push({ name: cell.v.trim(), start: col, end: col, sites: [] });
        } else if (companies.length) {
            companies[companies.length - 1].end = col;
        }
    }

    return companies;
}

function readSites(sheet, company, fileStructure) {
    for (let col = company.start; compareCols(col, company.end) <= 0; col = incCol(col)) {
        company.sites.push(readSite(sheet, col, fileStructure));
    }
    fixSiteNames(company.sites);

    return company;
}

/**
 *
 * @param {Record<string, unknown>[]} sites
 */
function fixSiteNames(sites) {
    const generatedNames = new Set();
    for (let site of sites) {
        if (site.siteName) {
            continue;
        }

        const communities = site.communities;
        let indexedCommunities;
        let index = 0;
        do {
            index++;
            const romanIndex = romanNumeral(index);
            indexedCommunities = `${communities} ${romanIndex}`;
        } while(generatedNames.has(indexedCommunities));

        site.siteName = indexedCommunities;
        generatedNames.add(indexedCommunities);
    }
}

const romanSegments = [
    ['I', 'II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII', 'IX'],
    ['X', 'XX', 'XXX', 'XL', 'L', 'LX', 'LXX', 'LXXX', 'XC'],
    ['C', 'CC', 'CCC', 'CD', 'D', 'DC', 'DCC', 'DCCC', 'CM'],
    ['M', 'MM', 'MMM']
];
function romanNumeral(num, power = 0, suffix = '') {
    const decimal = num % 10;
    const segment = decimal ? romanSegments[power][decimal - 1] : '';
    const nextSuffix = `${segment}${suffix}`;
    const nextNum = (num / 10) | 0;
    if (!nextNum) {
        return nextSuffix;
    }

    return romanNumeral(nextNum, power + 1, nextSuffix);
}

/**
 *
 * @param {import('xlsx/types').WorkSheet} sheet
 * @param {string} col
 * @returns {Record<string, unknown>}
 */
function readSite(sheet, col, fileStructure) {
    const site = {};
    const ret = fileStructure.reduce(assignField(sheet, col), site);

    return ret;
}

function assignField(sheet, col) {
    return function (site, field) {
        const cell = `${col}${field.row}`;
        const raw = sheet[cell] && sheet[cell].v;
        const defaultTransform = num();
        field.transform = eval(field.transform);
        const transformations = Array.isArray(field.transform)
            ? field.transform
            : [field.transform || defaultTransform];
        site[field.property] = transformations
            .reduce(
                (intermediate, fn) => fn(intermediate),
                raw === null || raw === undefined ? '' : raw
            );

        return site;
    };
}


function buildTable(companies, year, fileStructure) {
    return companies.reduce(
        addCompanyToTableFor(year),
        getEmptyTable(fileStructure)
    );
}

/**
 * @returns {[string[], ...unknown[][]]}
 */
function getEmptyTable(fileStructure) {
    return [[
        'id',
        'year',
        'societe',
        ...fileStructure.map(field => field.property)
    ]];
}

/**
 *
 * @param {number} year
 * @returns {(table: [string[], ...unknown[][]], company: Company) => [string[], ...unknown[][]]}
 */
function addCompanyToTableFor(year) {
    /**
     * @param {unknown[][]} table
     * @param {Company} company
     */
    return function addCompanyToTables(table, company) {
        company.sites.forEach(site => {
            const siteRecordId = uuidV1();
            const productionRecord = [
                siteRecordId,
                year,
                company.name,
                ...table[0].slice(3).map(prop => site[prop])
            ];

            table.push(productionRecord);
        });

        return table;
    };
}