/** Utilisé pour l'appliquation de plusieurs modification au même valeur, par ex.:
 * `flow(monthly(), fac(1000))` */
function flow(...fns) {
    return function(val) {
        return fns.reduce((intermediate, fn) => fn(intermediate), val);
    };
}

/** Multiplication par un facteur. Par ex. pour convertir les MW en kW utiliset `fac(1000)`,
 * ou pour l'inverse `fac(0.001)`. */
function fac(factor) {
    return function(val) {
        return (val || 0) * factor;
    };
}

/** Diviser par 12 pour arriver à un valeur moyen mensuel. Cette modification doit être appliquée
 * où un moyen peut être calculé de cette façon (par ex. pour la production) mais il faut
 * l'éviter pour des autres valeur où une telle calculation n'est pas faisable (par ex. la
 * puissance). */
function monthly() {
    return function(val) {
        return val / 12;
    };
}

module.exports = {
    fac,
    flow,
    monthly
};
