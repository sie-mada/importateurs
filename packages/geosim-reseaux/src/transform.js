const { join } = require('path');

const { tryLoadModule } = require('./util/try-require');

module.exports = {
    async transform(data, options) {
        const transformation = tryLoadModule(join(__dirname, 'transformations'), options.transformation);
        if (!transformation) {
            throw new Error(`Transformation ${options.transformation} not found`);
        }

        return transformation.transform(data, options);
    }
}
