const Zip = require('adm-zip');
const { readdir,  stat } = require('fs');
const { basename, join } = require('path');
const { open } = require('shapefile');
const { Readable } = require('stream');
const { promisify } = require('util');

const readdirP = promisify(readdir);
const statP = promisify(stat);

/**
 * @typedef {Object} GeoFeature
 * @property {string} $id
 * @property {import('geojson').Geometry} the_geom
 */

/**
 *
 * @param {string} src
 * @param {string=} dbf
 * @returns {Promise<object[]>}
 */
async function readShp(src, dbf) {
    if (dbf) {
        return doReadShp(src, dbf);
    } else if ((await statP(src)).isDirectory()) {
        const shpEnt = (await readdirP(src, { withFileTypes: true }))
            .find(dirent => dirent.isFile && dirent.name.endsWith('.shp'));
        if (!shpEnt) {
            throw new Error(`No .shp file found in directory ${src}`)
        }

        const shpPath = join(src, shpEnt.name);
        const shpBase = basename(shpEnt.name, '.shp');
        const dbfName = `${shpBase}.dbf`;
        const dbfPath = join(src, dbfName);

        return doReadShp(
            shpPath,
            (await statP(dbfPath)).isFile ? dbfPath : undefined
        );
    } else if (src.endsWith('.shp')) {
        const dbfPath = src.replace(/\.shp$/, '.dbf');
        return doReadShp(
            src,
            (await statP(dbfPath)).isFile ? dbfPath : undefined
        );
    } else if (src.endsWith('.zip')) {
        const zip = new Zip(src);
        const shpEntry = zip.getEntries().find(entry => entry.entryName.endsWith('.shp'));
        const dbfEntry = zip.getEntry(shpEntry.entryName.replace(/\.shp$/, '.dbf'));
        return doReadShp(shpEntry.getData(), dbfEntry ? dbfEntry.getData() : undefined);
    } else {
        throw new Error(`Don't know how to handle source "${src}"`);
    }
}

/**
 *
 * @param {string | Readable | ArrayBuffer | Uint8Array} src
 * @param {string | Readable | ArrayBuffer | Uint8Array=} dbf
 * @returns {Promise<GeoFeature[]>}
 */
async function doReadShp(src, dbf) {
    const shpData = await open(src, dbf);
    const data = [];

    for (let row = await shpData.read(); !row.done; row = await shpData.read()) {
        data.push({ $id: `${row.value.id}`, the_geom: row.value.geometry, ...row.value.properties });
    }

    return data;
}

module.exports = {
    readShp
};
