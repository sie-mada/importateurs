const { identity } = require('lodash/fp');

module.exports = {
    stack() {
        return new Error().stack
            .split('\n')
            .filter(row => /^\s*at /.test(row))
            .map(row => /at (?:([^ ]+) \(([^:]*):(\d+):(\d+)\)|([^:]*):(\d+):(\d+))$/.exec(row))
            .filter(identity)
            .map(match => ({ filename: match[2], functionName: match[1] || match[5], line: +(match[3] || match[6]), column: +(match[4] || match[7]) }))
            .slice(1);
    }
}
