const wkx = require('wkx');

module.exports = {
    transform(data) {
        const geom = wkx.Geometry
            .parseGeoJSON(data);
        geom.srid = 29702;
        return geom.toEwkt();
    }
};
