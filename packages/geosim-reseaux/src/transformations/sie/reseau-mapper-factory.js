const { transform: toEwkt } = require('../geo-transform/ewkt');

module.exports = {
    reseauMapperFactory(name, columnMapping) {

        function mapColumns(item) {
            return Object.entries(columnMapping)
                .reduce(
                    (obj, entry) => {
                        const targetKey = typeof entry[1] === 'string' ? entry[1] : entry[1][0];
                        const val = typeof entry[1] === 'string'
                            ? item[entry[0]]
                            : entry[1][1](item[entry[0]]);
                        return { ...obj, [targetKey]: val };
                    },
                    { the_geom: toEwkt(item.the_geom) }
                );
        }

        /**
         *
         * @param {Record<string, unknown>} item
         */
        function ensureReseauColumns(item) {
            return Array
                .from(Object.keys(columnMapping))
                .every(Object.prototype.hasOwnProperty.bind(item));
        }

        return {
            identify(data) {
                return ensureReseauColumns(data[0]);
            },

            map(data) {
                return data.map(mapColumns);
            },

            /**
             *
             * @param {Function} fn
             * @param {string=} target
             */
            patch(fn, target = 'map') {
                const prevFn = this[target];
                this[target] = function() {
                    return fn.call(this, prevFn, ...arguments);
                };

                return this;
            },

            get name() {
                return name;
            }
        };
    }
};
