const { reseauMapperFactory } = require("./reseau-mapper-factory");

/**
 * @type {Record<string, string | [string, (val: string) => any]>} */
const columnMappingReseauMt = {
    TENSION: ['tension', val => {
        const match = /(\d+)\s*(k?)V/.exec(val);
        if (!match) {
            return null;
        }

        return +match[1] * (match[2] === 'k' ? 1000 : 1);
    }],
    ID: 'id_reseau',
    LONGUEUR_M: ['longueur', Number],
    TYPE: ['type', val => reseauTypes.get((val || '').trim().toLowerCase()) || null],
    Statut: ['existant', val => /existant/i.test(val)]
};

const reseauTypes = new Map([
    ['aérien', 1],
    ['souterrain', 2],
    ['sous-marin', 3]
]);

module.exports = reseauMapperFactory('reseauMt', columnMappingReseauMt);
