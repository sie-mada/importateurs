const { reseauMapperFactory } = require("./reseau-mapper-factory");

/**
 * @type {Record<string, string | [string, (val: string) => any]>} */
const columnMappingReseauMt = {
    TENSION: ['tension', val => {
        const match = /(\d+)\s*((k?)V)?/.exec(val);
        if (!match) {
            return null;
        }

        return +match[1] * (!match[2] || match[3] === 'k' ? 1000 : 1);
    }],
    ID: 'id_reseau',
    Longueur: ['longueur', Number],
    TYPE: ['type', val => reseauTypes.get((val || '').toLowerCase()) || null],
    Statut: ['existant', val => /existant/i.test(val)]
};

const reseauTypes = new Map([
    ['arien', 1],
    ['aérien', 1],
    ['souterrain', 2],
    ['sous-marin', 3]
]);

module.exports = reseauMapperFactory('reseauExistant', columnMappingReseauMt);
