const { basename } = require('path');
const { v1: uuidV1 } = require('uuid');
const reseauHt = require('./sie/reseau_ht');
const reseauMt = require('./sie/reseau_mt');
const reseauExistant = require('./sie/reseau_existant');

const mappers = [
    reseauHt,
    reseauMt,
    reseauExistant
];

/**
 *
 * @param {import("../read-shp").GeoFeature[]} data
 * @param {Record<string, string>} options
 */
function transform(data, options) {
    if (!data || !Array.isArray(data) || !data.length) {
        throw new Error('Unexpected format of parameter "data"');
    }

    const accepting = mappers.find(mapper => mapper.identify(data));
    if (!accepting) {
        throw new Error('No mapper found for data structure');
    }

    const mappedData = accepting.map(data);
    const cols = Object.keys(mappedData[0]);
    const rows = mappedData.map(feature => cols.reduce((row, col) => (row.push(psqlify(feature[col], col)), row), []))

    const importId = uuidV1();
    const importFn = basename(options.source);
    const statements = [
        `DELETE FROM RESEAUX WHERE IMPORT = (SELECT id FROM imports WHERE filename = '${importFn}' ORDER BY "timestamp" DESC LIMIT 1);`,
        `INSERT INTO imports(id, "type", "timestamp", filename) VALUES ('${importId}', '${accepting.name}', NOW(), '${importFn}');`,
        `INSERT INTO reseaux(import, ${cols.map(col => `"${col}"`).join(',')}) VALUES\n${rows.map(row => `\t('${importId}',${row.join(',')})`).join(',\n')};`,
        `UPDATE reseaux SET longueur = ST_Length(the_geom) WHERE import = '${importId}' AND longueur = 0;`
    ];

    return statements.join('\n');
}

function psqlify(value, col) {
    if (typeof value === 'string') {
        return col === 'the_geom'
            ? `ST_Transform(ST_Multi('${value}'), 4326)`
            : `'${value.replace(/'/g, '\'\'')}'`;
    } else {
        return `${value}`;
    }
}

module.exports = {
    transform
};
