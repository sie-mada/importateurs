const winston = require('winston');

const initLogger = ( filename ) => {
    
    logger = new winston.Logger({
        level: 'debug',
        format: winston.format.simple(),
        transports: [
            new winston.transports.Console({
                stderrLevels: ['silly', 'debug', 'verbose', 'info', 'warn', 'error'],
                level: 'warn',
                format: winston.format.colorize({ all: true,  })
            }), 
            new winston.transports.File({
                filename,
                level: 'debug'
            })
        ]
    })
}

module.exports = { initLogger };
