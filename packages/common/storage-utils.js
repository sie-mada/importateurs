const fs = require('fs');
const path = require("path"); 
const { createWriteStream } = require('fs');


const saveFileStructure = (fileStructure, fileStructureFrench, importId, path) => {
    
    if (!fs.existsSync(path)){
        fs.mkdirSync(path, { recursive: true });
    }
    const directoryName = path + '/import_' + importId;
    if (!fs.existsSync(directoryName)){
        fs.mkdirSync(directoryName, { recursive: true });
    }
    const jsonFilePath = directoryName + '/structure_' + importId + '.json';

    const joinedObject = {
        FileStructure: fileStructure,
        FileStructureFrench: fileStructureFrench
    };

    const fileString = JSON.stringify(joinedObject);
    fs.writeFileSync( jsonFilePath, fileString);
}

const readFileStructure = (importId, path) => {

    const filePath = path + '/import_' + importId + '/structure_' + importId + '.json';

    console.error('readFileStructure, filePath: ', filePath);

    let structure = {};

    let rawData = fs.readFileSync(filePath);
    structure = JSON.parse(rawData);
    console.error('readFileStructure, structure: ', structure);

    // fs.readFile(filePath, 'utf8', function (err, data) {
    //     if (err) 
    //         throw err;
    //     structure = JSON.parse(data);
    // });

    const fileStructure = structure.FileStructure;
    const fileStructureFrench = structure.FileStructureFrench;
    
    return fileStructure;
} 
const pathForOutputForImportForKpi = (importId, path) => {

    const filePath = path + '/import_' + importId + '/start_import_copy_' + importId + '.txt';

    console.log('createdDestFile : ', filePath);
    return filePath;
}

const saveExcelFile = (importId, sourcePath, fileStoragePath) => {

    if (!fs.existsSync(fileStoragePath)){
        fs.mkdirSync(fileStoragePath, { recursive: true });
    }
    const directoryName = fileStoragePath + '/import_' + importId;
    if (!fs.existsSync(directoryName)){
        fs.mkdirSync(directoryName, { recursive: true });
    }
    const outputFileName = path.basename(sourcePath);
    const outputFileBaseName = path.parse(outputFileName).name;

    const excelFilePath = directoryName + '/' + outputFileBaseName + '_' + importId + '.xlsx';

    fs.copyFile(sourcePath, excelFilePath, (err) => {
        if (err) {
            console.log("Error saveExcelFile copy file failed: ", sourcePath, excelFilePath);
            throw err;
        }
    });
}

module.exports = { readFileStructure, saveExcelFile, saveFileStructure, pathForOutputForImportForKpi };
