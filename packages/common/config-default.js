const sharedVolumePath = "/var/lib/meeh-shell/importers/data/"
const fileStoragePath = "/var/lib/meeh-shell/importers/data/import-files"


module.exports = {
    sharedVolumePath: sharedVolumePath, 
    fileStoragePath: fileStoragePath,
};
