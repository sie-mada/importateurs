const { createReadStream, createWriteStream } = require('fs');

function getCliArguments (argv) {
	const commandName = argv[0];
	const srcArg = argv[1];
	const userName = argv[2];
	const importId = argv[3];
	const fileStructure = argv[4];
	const checkFile = argv[5]
	const destArg = argv[6];

	const arguments = {
		commandName,
		srcArg,
		userName,
		importId,
		fileStructure,
		checkFile,
		destArg
	};

	return arguments;
}

function initSrcAndDest (sourceName, destinationName) {
    //const srcArg = yargs.argv._[1];
    const src = createReadStream(sourceName);
    //const destArg = yargs.argv._[5];
    const dest = !destinationName
        ? destFromSource(sourceName)
        : destinationName === '-' //signal for writing to stdout which will be captured by shell-backend 
        ? process.stdout
        : createWriteStream(destinationName);

    return { src, dest };

}

function destFromSource(src) {
    const base = /^(.*\/)?([^\\\/]*?)(\.[^.\\\/]*)?$/.exec(src);
    return createWriteStream(`${base[1]}${base[2]}.sql`);
}

module.exports = {getCliArguments, initSrcAndDest};
